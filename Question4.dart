Column(
  mainAxisSize: MainAxisSize.max,
  children: [
    Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      decoration: BoxDecoration(
        color: Color(0xFFEEEEEE),
      ),
      child: Image.network(
        'https://picsum.photos/seed/132/600',
        width: 100,
        height: 100,
        fit: BoxFit.cover,
      ),
    ),
  ],
)

